public class Player {
   private char name;
   private int win;
   private int lose;
   private int draw;
   
   public Player(char name,int win, int lose, int draw){
       this.name = name;
       this.win = win;
       this.lose = lose;
       this.draw = draw;
   }
   public char getName(){
       return name;
   }
   public void setName(char name){
       this.name = name;
   }
   public int getWin(){
       return win;
   }
   public int getLose(){
       return lose;
   }
   public int getDraw(){
       return draw;
   }
   public void win(){
       this.win ++; 
   }
   public void lose(){
       this.lose ++;
   }
   public void draw(){
       this.draw ++;
   }
}