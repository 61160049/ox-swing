import java.util.*;
public class Table {
    private char data[][] = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private Player win;
    private int turn;
    
    public Table(Player o,Player x){
        this.o = o;
        this.x = x;
        Random rd = new Random();
            if(rd.nextInt(2)+1== 1){
                currentPlayer = o;
            }else{
                currentPlayer = x;
            }
        win = null;
    }
    public boolean setRowCol(int row, int col){
        if (data[row - 1][col - 1] == ('-')) {
            data[row - 1][col - 1] = currentPlayer.getName();
            turn++;
            return true;
        }
        return false;

    }
    public boolean checkWin(){
        for (int i = 0; i < 3; i++) {
            if (checkRow(i)) {
		return true;
            } else if (checkCol(i)) {
		return true;
            } else if (checkX1()) {
		return true;
            } else if (checkX2()) {
		return true;
            }
	}
	return false;
    }
    public boolean checkDraw(){
        for(int i = 0;i < 3;i++){
            for(int j = 0;j < 3;j++){
                if(data[i][j] == '-'){
                    return false;
                }
            }
        }
        win = null;
        o.draw();
        x.draw();
        return true;
    }
    public Player getWinner(){
        return win;
    }
    public void switchTurn(){
        currentPlayer = currentPlayer == o ? x : o;
    }
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    public char[][] getData(){
        return data;
    }
    public boolean checkRow(int row){
        for(int i = 0;i < 3; i++){
            if(data[row][i] != currentPlayer.getName()){
                return false;
            }
        }
        win = currentPlayer;
        if(win == o){
            o.win();
            x.lose();
        }else{
            o.lose();
            x.win();
        }
        return true;
    }
    public boolean checkCol(int col){
        for(int i = 0;i < 3; i++){
            if(data[i][col] != currentPlayer.getName()){
                return false;
            }
        }
        win = currentPlayer;
        if(win == o){
            o.win();
            x.lose();
        }else{
            o.lose();
            x.win();
        }
        return true;
    }
    public boolean checkX1(){
       for(int i = 0; i<3; i++){
            if(data[i][i] != currentPlayer.getName()){
                return false;
            }
        }
        win = currentPlayer;
        if(win == o){
            o.win();
            x.lose();
        }else{
            o.lose();
            x.win();
        }
        return true;
    }
    public boolean checkX2(){
        for(int i = 0; i<3; i++){
            if(data[2-i][0+i] != currentPlayer.getName()){
                return false;
            }
        }
        win = currentPlayer;
        if(win == o){
            o.win();
            x.lose();
        }else{
            o.lose();
            x.win();
        }
        return true;
    }
}